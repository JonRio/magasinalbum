<?php
/**
 * Fait la gestion des views
 *
 * @author Jonathan Rioux
 * @version 1.0
 */
class View {
    private $data = array();
    private $render = FALSE;

    public function __construct($template) {
      try {
        $file = ROOT . './app/views/' . strtolower($template) . '.php';
        if (file_exists($file)) {
            $this->render = $file;
        } else {
            throw new customException('Template ' . $template . ' not found!');
        }
      }
      catch (customException $e) {
        throw new customException($e->errorMessage());
        }
      }

    public function assign($variable, $value) {
      $this->data[$variable] = $value;
      }

    public function __destruct() {
      extract($this->data);
      require $this->render;
      }
    }
?>
