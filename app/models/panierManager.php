<?php
include ROOT."./app/models/panier.php";

class PaniersManager extends Connexion {

  public function __construct($db) {
    parent::__construct($db);
  }

  public function add(Panier $panier) {
    $q = parent::prepare('INSERT INTO Panier(nom, id_Commande) VALUES(:nom, :id_Commande)');
    $q->bindValue(':nom', $panier->nom());
    $q->bindValue(':id_Commande', $panier->idCommande(), PDO::PARAM_INT);
    return(parent::execute());
    }


  public function delete(Panier $panier) {
    parent::exec('DELETE FROM panier WHERE id = '.$panier->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id, nom, id_Commande FROM panier WHERE id = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);
    return new Panier($donnees);
    }

  public function getList() {
    $paniers = [];
    $q = parent::query('SELECT id, nom, id_Commande FROM panier ORDER BY nom');
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
      $paniers[] = new Panier($donnees);
      }
    return $paniers;
    }

  public function getListFromNom($nomToLookFor) {
    $paniers = [];


	if($typeToLookFor=="0")
		$query = parent::query("SELECT id, nom, id_Commande
    						FROM Panier p
    						ORDER BY nom");
	else
    	$query = parent::query("SELECT id, nom, id_Commande
    						FROM Panier p
    						WHERE '".$nomToLookFor."' = p.nom
    						ORDER BY nom");


    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){
       $panier = new Panier($donnees);
       $paniers[] = $panier;
      }
    return $paniers;
    }

  }
