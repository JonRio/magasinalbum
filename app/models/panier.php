<?php
class Panier implements JsonSerializable {

  	private $_id_panier;
  	private $_nom;
    private $_id_Commande;
      
  	public function __construct(array $data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

  public function hydrate(array $donnees) {
     foreach ($donnees as $key => $value) {
       // On récupère le nom du setter correspondant à l'attribut.
       $method = 'set'.ucfirst($key);
       // Si le setter correspondant existe.
       if (method_exists($this, $method)){
         // On appelle le setter.
         $this->$method($value);
         }
       }
     }

  	public function id()    { return $this->_id_panier; }
  	public function nom()   { return $this->_nom; }
  	public function idCommande()  { return $this->_id_Commande; }

  	public function setId_panier($id) {
    	$this->_id_usager = (int) $id;
    	}

  	public function setNom($nom) {
    	if (is_string($nom) && strlen($nom) <= 1024) {
      		$this->_nom = $nom;
      		}
    	}

	public function setIdCommande($_id_Commande) {
    	if (is_int($_id_Commande) && $_id_Commande > 0) {
      		$this->_id_Commande = $_id_Commande;
      		}
    	}

	public function jsonSerialize () {
        return array(
            'nom'=>$this->_nom,
            'type'=>$this->_id_Commande);
    	}
  }
