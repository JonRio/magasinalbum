<?php
class Type implements JsonSerializable {

  	private $_id_type;
  	private $_nom;
    private $_id_Usager;
      
  	public function __construct(array $data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

  public function hydrate(array $donnees) {
     foreach ($donnees as $key => $value) {
       // On récupère le nom du setter correspondant à l'attribut.
       $method = 'set'.ucfirst($key);
       // Si le setter correspondant existe.
       if (method_exists($this, $method)){
         // On appelle le setter.
         $this->$method($value);
         }
       }
     }

  	public function id()    { return $this->_id_type; }
  	public function nom()   { return $this->_nom; }
  	public function idUsager()  { return $this->_id_Usager; }

  	public function setId_type($id) {
    	$this->_id_usager = (int) $id;
    	}

  	public function setNom($nom) {
    	if (is_string($nom) && strlen($nom) <= 1024) {
      		$this->_nom = $nom;
      		}
    	}

	public function setIdUsager($_id_Usager) {
    	if (is_int($_id_Usager) && $_id_Usager > 0) {
      		$this->_id_Usager = $_id_Usager;
      		}
    	}

	public function jsonSerialize () {
        return array(
            'nom'=>$this->_nom,
            'type'=>$this->_id_Usager);
    	}
  }
