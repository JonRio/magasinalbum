<?php
include ROOT."./app/models/Individu.php";

class AlbumsManager extends Connexion {

  public function __construct($db) {
    parent::__construct($db);
  }

  public function add(Album $album) {
    $q = parent::prepare('INSERT INTO Album(nom, duree, sortie, label) VALUES(:nom, :duree, :sortie, :label)');
    $q->bindValue(':nom', $album->nom());
    $q->bindValue(':duree', $album->duree(), PDO::PARAM_INT);
    $q->bindValue(':sortie', $album->sortie());
    $q->bindValue(':label', $album->label());
    return(parent::execute());
    }


  public function delete(Album $album) {
    parent::exec('DELETE FROM album WHERE id = '.$album->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id, nom, duree, sortie, label FROM album WHERE id = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);
    return new Album($donnees);
    }

  public function getList() {
    $albums = [];
    $q = parent::query('SELECT id, nom, duree, sortie, label FROM album ORDER BY nom');
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
      $albums[] = new Album($donnees);
      }
    return $albums;
    }

  public function getListFromGenre($genreToLookFor) {
    $albums = [];


	if($genreToLookFor=="0")
		$query = parent::query("SELECT id, nom, duree, sortie, label
    						FROM Album a
    						ORDER BY nom");
	else
    	$query = parent::query("SELECT id, nom, duree, sortie, label
    						FROM Album a
    						WHERE '".$genreToLookFor."' = a.genre
    						ORDER BY nom");


    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){
       $album = new Album($donnees);
       $albums[] = $album;
      }
    return $albums;
    }

  }
