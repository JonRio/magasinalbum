<?php
include ROOT."./app/models/panier.php";

class CommandesManager extends Connexion {

  public function __construct($db) {
    parent::__construct($db);
  }

  public function add(Commande $commande) {
    $q = parent::prepare('INSERT INTO Commande(nom, id_Usager) VALUES(:nom, :id_Usager)');
    $q->bindValue(':nom', $panier->nom());
    $q->bindValue(':id_Usager', $panier->idUsager(), PDO::PARAM_INT);
    return(parent::execute());
    }


  public function delete(Commande $commande) {
    parent::exec('DELETE FROM commande WHERE id = '.$commande->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id, nom, id_Usager FROM commande WHERE id = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);
    return new Commande($donnees);
    }

  public function getList() {
    $commandes = [];
    $q = parent::query('SELECT id, nom, id_Usager FROM commande ORDER BY nom');
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
      $commandes[] = new Commande($donnees);
      }
    return $commandes;
    }

  public function getListFromNom($nomToLookFor) {
    $commandes = [];


	if($typeToLookFor=="0")
		$query = parent::query("SELECT id, nom, id_Usager
    						FROM Commande c
    						ORDER BY nom");
	else
    	$query = parent::query("SELECT id, nom, id_usager
    						FROM Commande c
    						WHERE '".$nomToLookFor."' = c.nom
    						ORDER BY nom");


    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){
       $commande = new Panier($donnees);
       $commandes[] = $commande;
      }
    return $commandes;
    }

  }
