<?php
include ROOT."./app/models/usager.php";

class TypesManager extends Connexion {

  public function __construct($db) {
    parent::__construct($db);
  }

  public function add(Type $type) {
    $q = parent::prepare('INSERT INTO type(nom, id_Usager) VALUES(:nom, :id_Usager)');
    $q->bindValue(':nom', $type->nom());
    $q->bindValue(':id_Usager', $type->idUsager(), PDO::PARAM_INT);
    return(parent::execute());
    }

  public function delete(Type $type) {
    parent::exec('DELETE FROM type WHERE id = '.$type->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id, nom, id_Usager FROM type WHERE id = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);
    return new Type($donnees);
    }

  public function getList() {
    $types = [];
    $q = parent::query('SELECT id, nom, id_Usager FROM type ORDER BY nom');
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
      $types[] = new Type($donnees);
      }
    return $types;
    }

  public function getListFromNom($nomToLookFor) {
    $types = [];


	if($nomToLookFor=="0")
		$query = parent::query("SELECT id, nom, id_Usager
    						FROM type t
    						ORDER BY nom");
	else
    	$query = parent::query("SELECT id, nom, id_Usager
    						FROM type t
    						WHERE '".$nomToLookFor."' = t.genre
    						ORDER BY nom");


    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){
       $type = new Type($donnees);
       $types[] = $type;
      }
    return $types;
    }

  }
