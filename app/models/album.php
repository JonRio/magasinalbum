<?php
class Album implements JsonSerializable {

  	private $_id_album;
  	private $_nom;
  	private $_duree;
    private $_sortie;
    private $_label;
  	public function __construct(array $data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

  public function hydrate(array $donnees) {
     foreach ($donnees as $key => $value) {
       // On récupère le nom du setter correspondant à l'attribut.
       $method = 'set'.ucfirst($key);
       // Si le setter correspondant existe.
       if (method_exists($this, $method)){
         // On appelle le setter.
         $this->$method($value);
         }
       }
     }

  	public function id()    { return $this->_id_album; }
  	public function nom()   { return $this->_nom; }
  	public function duree()  { return $this->_duree; }
    public function sortie()  { return $this->_sortie; }
    public function label()  { return $this->_label; }

  	public function setId_album($id) {
    	$this->_id_album = (int) $id;
    	}

  	public function setNom($nom) {
    	if (is_string($nom) && strlen($nom) <= 1024) {
      		$this->_nom = $nom;
      		}
    	}

	public function setDuree($duree) {
    	if (is_time($duree) && $duree > 0) {
      		$this->_duree = $duree;
      		}
    	}

 	public function setSortie($sortie) {
    	if (is_date($sortie)) {
      		$this->_sortie = $sortie;
      		}
        }
        
        public function setLabel($label) {
            if (is_string($label) && strlen($label) <= 1024) {
                  $this->_label = $label;
                  }
            }

	public function jsonSerialize () {
        return array(
            'nom'=>$this->_nom,
            'duree'=>$this->_duree,
            'sortie'=>$this->_sortie,
            'label'=>$this->_label);
    	}
  }
