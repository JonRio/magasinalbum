<?php
class Usager implements JsonSerializable {

  	private $_id_usager;
  	private $_nom;
  	private $_type;
    private $_identifiant;
    private $_password;
  	public function __construct(array $data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

  public function hydrate(array $donnees) {
     foreach ($donnees as $key => $value) {
       // On récupère le nom du setter correspondant à l'attribut.
       $method = 'set'.ucfirst($key);
       // Si le setter correspondant existe.
       if (method_exists($this, $method)){
         // On appelle le setter.
         $this->$method($value);
         }
       }
     }

  	public function id()    { return $this->_id_album; }
  	public function nom()   { return $this->_nom; }
  	public function type()  { return $this->_type; }
    public function identifiant()  { return $this->_identifiant; }
    public function password()  { return $this->_password; }

  	public function setId_usager($id) {
    	$this->_id_usager = (int) $id;
    	}

  	public function setNom($nom) {
    	if (is_string($nom) && strlen($nom) <= 1024) {
      		$this->_nom = $nom;
      		}
    	}

	public function setType($type) {
    	if (is_int($type) && $type > 0) {
      		$this->_type = $type;
      		}
    	}

 	public function setidentifiant($identifiant) {
    	if (is_string($identifiant) && strlen($identifiant) <= 1024) {
      		$this->_identifiant = $identifiant;
      		}
        }
        
        public function setpassword($password) {
            if (is_string($password) && strlen($password) <= 1024) {
                  $this->_password = $password;
                  }
            }

	public function jsonSerialize () {
        return array(
            'nom'=>$this->_nom,
            'type'=>$this->_type,
            'identifiant'=>$this->_identifiant,
            'password'=>$this->_password);
    	}
  }
