<?php
/**
 * Fait la gestion connexions
 *
 * @author Jonathan Rioux
 * @version 1.0
 */
class Connexion {

 /**
	*
	* @var PDO Est le pointeur sur un objet PDO
    */
  private $_connection;

  /**
	*
	* @var string Est le nom de l'hote qui contient la base de données
    */
  private $_host = DB_HOST;

  /**
	*
	* @var string Est le nom de l'usager
    */
  private $_user = DB_USER;

  /**
	*
	* @var string Est le mot de passe de l'usager
    */
  private $_pass = DB_PASS;

  /**
	*
	* @var int Est le port a utiliser pour l'acces à la base de données
    */
  private $_port = DB_PORT;

  /**
     * Constructeur de la classe
     * @param string $db  Est le nom de la base de données
     */
  public function __construct($db) {
   	try {
    	$conn= new PDO("mysql:host=".$this->_host.";port=".$this->_port.";dbname=". $db.";charset=utf8", $this->_user, $this->_pass);
    	// set the PDO error mode to exception
    	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	$this->connection = $conn;
    	}
	catch(PDOException $e) {
      if (class_exists('ChromePhp')) ChromePhp::log("Impossible de se connecter à la base de données ..." . $e->getMessage( ));
    	throw new Exception('Impossible de se connecter à la base de données');
    	$this->connection = null;
    	}
  	}

   /**
     * Appel la méthode query de l'objet PDO
     * @param string $sql  Est la requête
     * @return PDOStatement Retourne un jeu de résultat
     */
  public function query($sql) {



    return $this->connection->query($sql);
  	}

   /**
     * Appel la méthode exec de l'objet PDO
     * @param string $sql  Est la requête
     * @return int Retourne le nombre de lignes affectés
     */
  public function exec($sql) {
    return $this->connection->exec($sql);
  	}



     /**
       * Appel la méthode execute de l'objet PDO
       * et retourne le dernier ID ajouté
       * @return string Retourne le dernier Id Ajouté si disponible
       */
    public function execute($q) {
      $q->execute();
      $dernierIdAdded = $this->connection->lastInsertId();
      return $dernierIdAdded;
      }

  /**
     * Appel la méthode prepare de l'objet PDO
     * @param string $sql  Est la requête
     * @return PDOStatement Retourne un objet
     */
  public function prepare($sql) {
    return $this->connection->prepare($sql);
  	}
}
