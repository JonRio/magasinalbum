<?php

// autoload.php @generated by Composer

require_once __DIR__ . '/composer/autoload_real.php';

define('BASEDIR', __DIR__ .'/../../');
define('ROOT', __DIR__ .'/../../');
define('DB_TYPE', 'mysql');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', 8080);
define('DB_NAME', 'memt4103_1134441');
define('DB_USER', 'root');
define('DB_PASS', 'root');

return ComposerAutoloaderInit0e7957a94816a5b2687bba2466d51600::getLoader();
